# basic

Dieses Beispiel zeigt, wie man Images, Jobs und Stages in einer CI-Pipeline verwendet.

- Da die CI-Infrastruktur der OEV kubernetes verwendet ist das image tag immer relevant. Wenn es weg gelassen wird gibt es jedoch ein default Image.
- Jobs innerhalb der gleichen Stage laufen parallel ab.
- Die Jobs der jeweils nächsten Stage laufen erst dann, wenn alle Jobs der vorigen stage erfolgreich beendet wurden.

Weitere Infromationen zu den Keywords:
- [image](https://docs.gitlab.com/ee/ci/yaml/#image)
- [job](https://docs.gitlab.com/ee/ci/yaml/#introduction)
- [stages](https://docs.gitlab.com/ee/ci/yaml/#stages)
